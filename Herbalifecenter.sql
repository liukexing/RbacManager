-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.5.5-10.0.17-MariaDB - mariadb.org binary distribution
-- 服务器操作系统:                      Win32
-- HeidiSQL 版本:                  8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- 正在导出表  herbalifecenter.action 的数据：~29 rows (大约)
/*!40000 ALTER TABLE `action` DISABLE KEYS */;
INSERT INTO `action` (`id`, `pid`, `controller`, `action`, `des`, `create`, `updated`, `count`, `group`, `ispublc`, `Actiontype`) VALUES
	(5, 0, 'RootController', 'Index', '', '2016-07-06 18:15:37', '2017-01-24 03:22:22', 425, 'controllers', 1, 2),
	(7, 0, 'RootController', 'Login', '', '2016-08-18 02:17:03', '2017-01-24 03:19:03', 298, 'controllers', 1, 2),
	(8, 0, 'RootController', 'CheckUser', '', '2016-09-06 02:17:05', '2017-01-24 03:19:05', 241, 'controllers', 1, 1),
	(10, 7, 'RootController', 'LoginOut', '', '2016-11-17 21:59:30', '2017-01-24 03:19:03', 23, 'controllers', 0, 1),
	(14, 5, 'UserController', 'Index', '', '2016-09-05 01:52:38', '2017-01-24 03:21:55', 259, 'Admin', 0, 1),
	(15, 5, 'UserController', 'UserList', '', '2016-04-11 09:13:35', '2017-01-24 03:21:55', 705, 'Admin', 0, 2),
	(16, 5, 'UserController', 'RoleList', '', '0000-00-00 00:00:00', '2017-01-24 03:20:15', 143, 'Admin', 0, 2),
	(17, 5, 'UserController', 'AddUser', '', '2016-11-24 01:30:40', '2016-12-07 05:18:04', 37, 'Admin', 0, 1),
	(18, 5, 'UserController', 'UpdateUser', '', '2016-11-11 02:41:02', '2016-12-07 08:24:01', 76, 'Admin', 0, 1),
	(19, 5, 'UserController', 'DelUser', '', '2016-12-03 20:35:22', '2016-12-06 08:03:50', 8, 'Admin', 0, 1),
	(20, 5, 'UserController', 'GetUser', '', '2016-11-26 14:47:23', '2016-12-08 08:19:04', 30, 'Admin', 0, 1),
	(21, 5, 'UserController', 'UpdateRole', '', '2016-12-04 23:20:29', '2016-12-07 08:05:53', 8, 'Admin', 0, 1),
	(22, 5, 'UserController', 'AddRole', '', '2016-12-04 15:45:18', '2016-12-07 07:50:22', 9, 'Admin', 0, 1),
	(23, 5, 'UserController', 'GetRole', '', '2016-12-05 23:47:50', '2016-12-08 08:24:07', 5, 'Admin', 0, 1),
	(24, 5, 'UserController', 'DelRole', '', '2016-12-06 23:52:48', '2016-12-08 04:20:27', 2, 'Admin', 0, 1),
	(25, 5, 'UserController', 'UserAccess', '', '2016-10-08 08:13:50', '2017-01-24 03:20:36', 181, 'Admin', 0, 2),
	(26, 5, 'UserController', 'AddRoleUser', '', '2016-11-26 18:50:33', '2016-12-09 07:45:11', 35, 'Admin', 0, 1),
	(27, 5, 'UserController', 'DelRoleUser', '', '2016-11-28 10:51:05', '2016-12-12 08:59:50', 30, 'Admin', 0, 1),
	(28, 5, 'NodeController', 'Index', '', '2016-11-25 00:42:41', '2017-01-24 03:21:58', 41, 'Admin', 0, 1),
	(29, 5, 'NodeController', 'MenuList', '', '2016-10-13 00:49:38', '2017-01-24 03:21:58', 170, 'Admin', 0, 2),
	(30, 5, 'NodeController', 'LinkList', '', '2016-11-27 08:51:56', '2017-01-24 03:22:00', 34, 'Admin', 0, 2),
	(31, 5, 'NodeController', 'AddNode', '', '2016-12-05 10:57:03', '2016-12-12 05:59:27', 12, 'Admin', 0, 1),
	(32, 5, 'NodeController', 'DelNode', '', '2016-12-08 19:11:29', '2016-12-09 08:17:35', 2, 'Admin', 0, 1),
	(33, 5, 'NodeController', 'GetNode', '', '2016-12-06 05:29:56', '2016-12-12 05:42:07', 10, 'Admin', 0, 1),
	(34, 5, 'NodeController', 'UpdateNode', '', '2016-12-07 05:44:15', '2016-12-12 05:42:11', 7, 'Admin', 0, 1),
	(35, 5, 'UserController', 'RoleAccess', '', '2016-11-30 13:43:29', '2017-01-24 03:21:56', 36, 'Admin', 0, 2),
	(36, 5, 'UserController', 'AddRoleNode', '', '2016-12-10 13:55:39', '2016-12-12 06:27:13', 6, 'Admin', 0, 1),
	(37, 5, 'UserController', 'DelRoleNode', '', '2016-12-12 06:03:49', '2016-12-12 06:03:49', 1, 'Admin', 0, 1),
	(38, 0, 'RootController', 'NotAccess', '', '2016-12-10 09:00:02', '2016-12-12 09:47:00', 7, 'controllers', 1, 2);
/*!40000 ALTER TABLE `action` ENABLE KEYS */;

-- 正在导出表  herbalifecenter.agent 的数据：~7 rows (大约)
/*!40000 ALTER TABLE `agent` DISABLE KEYS */;
INSERT INTO `agent` (`id`, `useragent`, `agentjson`, `create`, `updated`, `count`) VALUES
	(1, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0', '{"agent_type":"Browser","agent_name":"Firefox","agent_version":"49.0","os_type":"Windows","os_name":"Windows 7","os_versionName":"","os_versionNumber":"","os_producer":"","os_producerURL":"","linux_distibution":"Null","agent_language":"","agent_languageTag":""}', '2016-11-17 02:26:46', '2016-11-30 03:18:29', 108),
	(2, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '{"agent_type":"Browser","agent_name":"Firefox","agent_version":"50.0","os_type":"Windows","os_name":"Windows 7","os_versionName":"","os_versionNumber":"","os_producer":"","os_producerURL":"","linux_distibution":"Null","agent_language":"","agent_languageTag":""}', '2016-12-01 01:10:17', '2017-01-24 03:19:03', 142),
	(3, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '{"agent_type":"Browser","agent_name":"Chrome","agent_version":"40.0.2214.94","os_type":"Windows","os_name":"Windows 7","os_versionName":"","os_versionNumber":"","os_producer":"","os_producerURL":"","linux_distibution":"Null","agent_language":"","agent_languageTag":""}', '2016-12-06 08:10:46', '2016-12-07 06:35:12', 4),
	(4, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 UBrowser/6.0.895.26 Safari/537.36', '{"agent_type":"Browser","agent_name":"Chrome","agent_version":"50.0.2661.102","os_type":"Windows","os_name":"Windows 7","os_versionName":"","os_versionNumber":"","os_producer":"","os_producerURL":"","linux_distibution":"Null","agent_language":"","agent_languageTag":""}', '2016-12-09 07:39:54', '2016-12-09 07:39:54', 0),
	(5, 'Mozilla/5.0 (Linux; Android 6.0.1; A0001 Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile MQQBrowser/6.8 TBS/036887 Safari/537.36 V1_AND_SQ_6.6.0_432_YYB_D QQ/6.6.0.2935 NetType/WIFI WebP/0.3.0 Pixel/1080', '{"agent_type":"Browser","agent_name":"Android Webkit Browser","agent_version":"--","os_type":"Android","os_name":"Android","os_versionName":"","os_versionNumber":"6.0.1","os_producer":"","os_producerURL":"","linux_distibution":"Null","agent_language":"","agent_languageTag":""}', '2016-12-09 07:40:20', '2016-12-09 07:40:25', 1),
	(6, 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; InfoPath.3)', '{"agent_type":"Browser","agent_name":"Internet Explorer","agent_version":"8.0","os_type":"Windows","os_name":"Windows 7","os_versionName":"","os_versionNumber":"","os_producer":"","os_producerURL":"","linux_distibution":"Null","agent_language":"","agent_languageTag":""}', '2016-12-09 07:42:06', '2016-12-09 07:42:12', 1),
	(7, 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; InfoPath.3)', '{"agent_type":"Browser","agent_name":"Internet Explorer","agent_version":"8.0","os_type":"Windows","os_name":"Windows 7","os_versionName":"","os_versionNumber":"","os_producer":"","os_producerURL":"","linux_distibution":"Null","agent_language":"","agent_languageTag":""}', '2016-12-09 07:42:09', '2016-12-09 07:42:12', 1);
/*!40000 ALTER TABLE `agent` ENABLE KEYS */;

-- 正在导出表  herbalifecenter.group 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` (`id`, `name`, `title`, `status`, `sort`) VALUES
	(1, 'App', 'System', 2, 1),
	(2, 'Admin', 'Rbac Manager', 2, 1);
/*!40000 ALTER TABLE `group` ENABLE KEYS */;

-- 正在导出表  herbalifecenter.node 的数据：~12 rows (大约)
/*!40000 ALTER TABLE `node` DISABLE KEYS */;
INSERT INTO `node` (`id`, `title`, `name`, `level`, `pid`, `remark`, `status`, `group_id`, `pic`, `sort`) VALUES
	(1, '内容管理', 'User.UserController.Index', 1, 0, '内容管理', 2, 2, NULL, NULL),
	(2, '模块管理', 'Admin.NodeController.Index', 1, 0, '模块管理', 2, 2, NULL, NULL),
	(3, '用户管理', 'Admin.UserController.Index', 1, 0, '用户管理', 2, 2, NULL, NULL),
	(4, '菜单管理', 'Admin.NodeController.MenuList', 2, 2, '菜单管理', 2, 2, NULL, NULL),
	(5, '导航管理', 'Admin.NodeController.LinkList', 2, 2, '导航管理', 2, 2, NULL, NULL),
	(7, '用户列表', 'Admin.UserController.UserList', 2, 3, '用户列表', 2, 2, NULL, NULL),
	(8, '角色列表', 'Admin.UserController.RoleList', 2, 3, '角色列表', 2, 2, NULL, NULL),
	(9, '用户授权', 'Admin.UserController.UserAccess', 2, 3, '用户授权', 2, 2, NULL, NULL),
	(11, '博客分类', 'User.UserController.TypeList', 2, 1, '博客分类', 2, 2, NULL, NULL),
	(12, 'test', 'test', 1, 0, '1111111', 2, 1, NULL, NULL),
	(13, '角色授权', 'Admin.UserController.RoleAccess', 2, 3, '角色授权', 2, 2, NULL, NULL),
	(14, '页面管理', '', 2, 2, '页面管理', 2, 2, NULL, NULL);
/*!40000 ALTER TABLE `node` ENABLE KEYS */;

-- 正在导出表  herbalifecenter.node_roles 的数据：~15 rows (大约)
/*!40000 ALTER TABLE `node_roles` DISABLE KEYS */;
INSERT INTO `node_roles` (`id`, `node_id`, `role_id`) VALUES
	(1, 1, 1),
	(2, 2, 1),
	(3, 3, 1),
	(4, 4, 1),
	(5, 5, 1),
	(6, 6, 1),
	(7, 7, 1),
	(8, 8, 1),
	(9, 9, 1),
	(10, 13, 1),
	(12, 11, 1),
	(13, 14, 1),
	(14, 12, 5),
	(15, 1, 5),
	(16, 11, 5);
/*!40000 ALTER TABLE `node_roles` ENABLE KEYS */;

-- 正在导出表  herbalifecenter.role 的数据：~10 rows (大约)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `title`, `name`, `remark`, `status`) VALUES
	(1, 'Admin role', 'Admin', 'I\'m a admin role', 2),
	(3, 'Customer', 'Customer1', 'I\'m a Customer role', 2),
	(4, '11', '11', '11', 2),
	(5, '22', '22', '22', 2),
	(6, '33', '33', '33', 2),
	(7, '44', '44', '44', 2),
	(8, '55', '55', '55', 2),
	(9, '66', '66', '66', 2),
	(10, '77', '77', '77', 2),
	(11, '88', '88', '88', 2);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- 正在导出表  herbalifecenter.user 的数据：~22 rows (大约)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`, `nickname`, `email`, `remark`, `status`, `lastlogintime`, `createtime`) VALUES
	(1, 'admin', '21218cca77804d2ba1922c33e0151105', 'admin', 'admin@123.com', 'I am admin', 2, '2016-12-02 15:49:21', '0000-00-00 00:00:00'),
	(12, 'user1', '21218cca77804d2ba1922c33e0151105', '用户1', '912343876@qq.cm', '谁谁谁222', 2, '2016-12-06 06:07:10', '2016-12-06 06:07:10'),
	(14, 'user3', '21218cca77804d2ba1922c33e0151105', '用户3', '123456', '谁谁谁', 2, '2016-12-06 07:23:29', '2016-12-06 07:23:29'),
	(15, '1', '21218cca77804d2ba1922c33e0151105', '1', '', '', 2, '2016-12-07 00:39:26', '2016-12-07 00:39:26'),
	(16, '2', '21218cca77804d2ba1922c33e0151105', '2', '', '', 2, '2016-12-07 00:39:36', '2016-12-07 00:39:36'),
	(17, '3', '21218cca77804d2ba1922c33e0151105', '3', '', '', 2, '2016-12-07 00:39:45', '2016-12-07 00:39:45'),
	(18, '4', '21218cca77804d2ba1922c33e0151105', '4', '', '', 2, '2016-12-07 00:39:57', '2016-12-07 00:39:57'),
	(19, '5', '21218cca77804d2ba1922c33e0151105', '5', '', '', 2, '2016-12-07 00:40:04', '2016-12-07 00:40:04'),
	(20, '6', '21218cca77804d2ba1922c33e0151105', '6', '', '', 2, '2016-12-07 00:40:12', '2016-12-07 00:40:12'),
	(21, '7', '21218cca77804d2ba1922c33e0151105', '7', '', '', 2, '2016-12-07 00:40:23', '2016-12-07 00:40:23'),
	(22, '8', '21218cca77804d2ba1922c33e0151105', '8', '', '', 2, '2016-12-07 00:40:35', '2016-12-07 00:40:35'),
	(23, '9', '21218cca77804d2ba1922c33e0151105', '9', '', '', 2, '2016-12-07 00:40:44', '2016-12-07 00:40:44'),
	(24, '10', '21218cca77804d2ba1922c33e0151105', '10', '', '', 2, '2016-12-07 00:40:53', '2016-12-07 00:40:53'),
	(25, '11', '21218cca77804d2ba1922c33e0151105', '11', '', '', 2, '2016-12-07 05:16:21', '2016-12-07 05:16:21'),
	(26, '12', '21218cca77804d2ba1922c33e0151105', '12', '', '', 2, '2016-12-07 05:17:07', '2016-12-07 05:17:07'),
	(27, '13', '21218cca77804d2ba1922c33e0151105', '13', '', '', 2, '2016-12-07 05:17:15', '2016-12-07 05:17:15'),
	(28, '14', '21218cca77804d2ba1922c33e0151105', '14', '', '', 2, '2016-12-07 05:17:23', '2016-12-07 05:17:23'),
	(29, '15', '21218cca77804d2ba1922c33e0151105', '15', '', '', 2, '2016-12-07 05:17:30', '2016-12-07 05:17:30'),
	(30, '16', '21218cca77804d2ba1922c33e0151105', '16', '', '', 2, '2016-12-07 05:17:36', '2016-12-07 05:17:36'),
	(31, '17', '21218cca77804d2ba1922c33e0151105', '17', '', '', 1, '2016-12-07 05:17:45', '2016-12-07 05:17:45'),
	(32, '18', '21218cca77804d2ba1922c33e0151105', '18', '', '', 1, '2016-12-07 05:17:54', '2016-12-07 05:17:54'),
	(33, '19', '21218cca77804d2ba1922c33e0151105', '19', '', '', 1, '2016-12-07 05:18:04', '2016-12-07 05:18:04');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- 正在导出表  herbalifecenter.user_roles 的数据：~6 rows (大约)
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` (`id`, `role_id`, `user_id`) VALUES
	(1, 1, 1),
	(2, 2, 1),
	(9, 12, 14),
	(11, 1, 12),
	(28, 7, 14),
	(29, 4, 14);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
