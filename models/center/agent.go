package center

import (
	"github.com/astaxie/beego/orm"
	"time"
)

type Agent struct {
	Id        int       `orm:"column(id);auto"`
	Useragent string    `orm:"column(useragent);type(text)"`
	Agentjson string    `orm:"column(agentjson);type(text)"`
	Create    time.Time `orm:"column(create);auto_now_add;type(datetime)"`
	Updated   time.Time `orm:"column(updated);auto_now;type(datetime)"`
	Count     int       `orm:"colum(count);"`
}

func (t *Agent) TableName() string {
	return "agent"
}

func init() {
	//orm.RegisterModelWithPrefix("uc_", new(Agent))
	orm.RegisterModel(new(Agent))
}

func AddUcAgent(agent string, json string) (id int64, err error) {
	o := orm.NewOrm()
	a := Agent{Useragent: agent, Agentjson: json}
	id, err = o.Insert(&a)
	return
}

func FindUcAgent(agent string) (json string, err error) {
	o := orm.NewOrm()
	t := Agent{}
	err = o.QueryTable(&t).Filter("Useragent", agent).One(&t)
	if err == nil {
		json = t.Agentjson
	}
	return
}

func IncUcAgent(agent string) {
	o := orm.NewOrm()
	t := Agent{}
	o.QueryTable(&t).Filter("Useragent", agent).Update(orm.Params{
		"Count":   orm.ColValue(orm.ColAdd, 1),
		"Updated": time.Now(),
	})
}
