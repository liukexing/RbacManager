#RbacManager
基于 beego + flatUI + mysql 的一套rbac用户权限管理系统
## 导航页面
![./doc/导航页.png](./doc/导航页.png)
## 菜单管理
![./doc/菜单管理.png](./doc/菜单管理.png)
## 导航管理
![./doc/导航管理.png](./doc/导航管理.png)
## 用户管理
![./doc/用户管理.png](./doc/用户管理.png)
## 角色管理
![./doc/角色管理.png](./doc/角色管理.png)
## 用户授权
![./doc/用户授权.png](./doc/用户授权.png)
## 角色授权
![./doc/角色授权.png](./doc/角色授权.png)