package Admin

import (
	m "git.oschina.net/liukexing/RbacManager/models/center"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
)

type NodeController struct {
	CommonController
}

func (n *NodeController) NestPrepare() {
	group := "Admin"
	controller, action := n.GetControllerAndAction()
	for _, node := range n.nodes {
		if node["Name"].(string) == (group + "." + controller + "." + action) {
			n.Data["Subid"] = node["Id"]
			n.Data["Menuid"] = node["Pid"]
		}
	}
	var Submenu []orm.Params
	for _, node := range n.nodes {
		if node["Level"].(int64) == 2 && node["Pid"].(int64) == n.Data["Menuid"].(int64) {
			Submenu = append(Submenu, node)
		}
	}
	n.Data["Submenu"] = Submenu
	n.Data["Title"] = "后台管理-用户管理"
}

func (n *NodeController) URLMapping() {
	n.Mapping("Index", n.Index)
	n.Mapping("MenuList", n.MenuList)
	n.Mapping("LinkList", n.LinkList)
	n.Mapping("AddNode", n.AddNode)
	n.Mapping("UpdateNode", n.UpdateNode)
	n.Mapping("DelNode", n.DelNode)
	n.Mapping("GetNode", n.GetNode)
}

// @router /node/index [get]
func (n *NodeController) Index() {
	n.Redirect(n.URLFor("Admin.NodeController.MenuList"), 302)
}

// @router /node/menulist [get]
func (n *NodeController) MenuList() {
	nodelist, _ := m.GetNodelistByGroupid(2)
	var firstlv []orm.Params
	for _, r1 := range nodelist {
		if r1["Level"].(int64) == 1 {
			var secondlv []orm.Params
			for _, r2 := range nodelist {
				if r2["Level"].(int64) == 2 && r2["Pid"].(int64) == r1["Id"].(int64) {
					secondlv = append(secondlv, r2)
				}
			}
			r1["children"] = secondlv
			firstlv = append(firstlv, r1)
		}
	}
	n.Data["showsearch"] = false
	n.Data["griddata"] = firstlv
}

// @router /node/linklist [get]
func (n *NodeController) LinkList() {
	nodelist, _ := m.GetNodelistByGroupid(1)
	var firstlv []orm.Params
	for _, r1 := range nodelist {
		if r1["Level"].(int64) == 1 {
			var secondlv []orm.Params
			for _, r2 := range nodelist {
				if r2["Level"].(int64) == 2 && r2["Pid"].(int64) == r1["Id"].(int64) {
					secondlv = append(secondlv, r2)
				}
			}
			r1["children"] = secondlv
			firstlv = append(firstlv, r1)
		}
	}
	n.Data["showsearch"] = false
	n.Data["griddata"] = firstlv
}

// @router /node/getnode [get]
func (n *NodeController) GetNode() {
	id, _ := n.GetInt64("id")
	o := orm.NewOrm()
	node := m.Node{Id: id}
	o.Read(&node)
	n.Data["json"] = &node
	n.ServeJSON()
}

// @router /node/addnode [post]
func (n *NodeController) AddNode() {
	flash := beego.NewFlash()
	result := true
	o := m.Node{}
	if err := n.ParseForm(&o); err != nil {
		flash.Error(err.Error())
		result = false
	}
	Groupid, _ := n.GetInt64("Groupid")
	o.Group = &m.Group{Id: Groupid}
	if _, err := m.AddNode(&o); err != nil {
		flash.Error(err.Error())
		result = false
	}
	if result {
		flash.Success("添加成功！")
	}
	flash.Store(&n.Controller)
	if Groupid == 2 {
		n.Redirect(n.URLFor("Admin.NodeController.MenuList"), 302)
	} else {
		n.Redirect(n.URLFor("Admin.NodeController.LinkList"), 302)
	}
}

// @router /node/updatenode [post]
func (n *NodeController) UpdateNode() {
	flash := beego.NewFlash()
	result := true
	o := m.Node{}
	if err := n.ParseForm(&o); err != nil {
		flash.Error(err.Error())
		result = false
	}
	Groupid, _ := n.GetInt64("Groupid")
	if _, err := m.UpdateNode(&o); err != nil {
		flash.Error(err.Error())
		result = false
	}
	if result {
		flash.Success("更新成功！")
	}
	flash.Store(&n.Controller)
	if Groupid == 2 {
		n.Redirect(n.URLFor("Admin.NodeController.MenuList"), 302)
	} else {
		n.Redirect(n.URLFor("Admin.NodeController.LinkList"), 302)
	}
}

// @router /node/delnode [post]
func (n *NodeController) DelNode() {
	flash := beego.NewFlash()
	result := true
	id, err := n.GetInt64("Id")
	Groupid, _ := n.GetInt64("Groupid")
	if err != nil {
		flash.Error(err.Error())
		result = false
	}
	if _, err := m.DelNodeById(id); err != nil {
		flash.Error(err.Error())
		result = false
	}
	if result {
		flash.Success("删除成功！")
	}
	flash.Store(&n.Controller)
	if Groupid == 2 {
		n.Redirect(n.URLFor("Admin.NodeController.MenuList"), 302)
	} else {
		n.Redirect(n.URLFor("Admin.NodeController.LinkList"), 302)
	}
}
