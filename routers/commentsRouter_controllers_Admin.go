package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:NodeController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:NodeController"],
		beego.ControllerComments{
			Method: "Index",
			Router: `/node/index`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:NodeController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:NodeController"],
		beego.ControllerComments{
			Method: "MenuList",
			Router: `/node/menulist`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:NodeController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:NodeController"],
		beego.ControllerComments{
			Method: "LinkList",
			Router: `/node/linklist`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:NodeController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:NodeController"],
		beego.ControllerComments{
			Method: "GetNode",
			Router: `/node/getnode`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:NodeController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:NodeController"],
		beego.ControllerComments{
			Method: "AddNode",
			Router: `/node/addnode`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:NodeController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:NodeController"],
		beego.ControllerComments{
			Method: "UpdateNode",
			Router: `/node/updatenode`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:NodeController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:NodeController"],
		beego.ControllerComments{
			Method: "DelNode",
			Router: `/node/delnode`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "Index",
			Router: `/user/index`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "UserList",
			Router: `/user/userlist`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "AddUser",
			Router: `/user/adduser`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "UpdateUser",
			Router: `/user/updateuser`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "DelUser",
			Router: `/user/deluser`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "GetUser",
			Router: `/user/getuser`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "RoleList",
			Router: `/user/rolelist`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "GetRole",
			Router: `/user/getrole`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "AddRole",
			Router: `/user/addrole`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "UpdateRole",
			Router: `/user/updaterole`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "DelRole",
			Router: `/user/delrole`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "UserAccess",
			Router: `/user/useraccess`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "AddRoleUser",
			Router: `/user/addroleuser`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "DelRoleUser",
			Router: `/user/delroleuser`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "RoleAccess",
			Router: `/user/roleaccess`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "AddRoleNode",
			Router: `/user/addrolenode`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers/Admin:UserController"],
		beego.ControllerComments{
			Method: "DelRoleNode",
			Router: `/user/delrolenode`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

}
