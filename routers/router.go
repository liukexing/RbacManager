package routers

import (
	"git.oschina.net/liukexing/RbacManager/controllers"
	"git.oschina.net/liukexing/RbacManager/controllers/Admin"
	//"git.oschina.net/liukexing/RbacManager/controllers/User"
	"github.com/astaxie/beego"
)

func init() {
	beego.SetStaticPath("/FlatUI", "static/Plugins/FlatUI")
	beego.SetStaticPath("/HubSpot", "static/Plugins/HubSpot")
	beego.Include(
		&controllers.RootController{},
	)
	admin := beego.NewNamespace("/admin",
		beego.NSInclude(
			&Admin.UserController{},
			&Admin.NodeController{},
		),
	)
	beego.AddNamespace(admin)
	// user := beego.NewNamespace("/User",
	// 	beego.NSRouter("/Index", &User.IndexController),
	// )
	// admin := beego.NewNamespace("/Admin",
	// 	beego.NSRouter("/Index", &Admin.IndexController),
	// )
	// beego.AddNamespace(user, admin)
}
