package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers:RootController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers:RootController"],
		beego.ControllerComments{
			Method: "Index",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers:RootController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers:RootController"],
		beego.ControllerComments{
			Method: "Login",
			Router: `/login`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers:RootController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers:RootController"],
		beego.ControllerComments{
			Method: "LoginOut",
			Router: `/loginout`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers:RootController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers:RootController"],
		beego.ControllerComments{
			Method: "CheckUser",
			Router: `/accessuser`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers:RootController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers:RootController"],
		beego.ControllerComments{
			Method: "Error",
			Router: `/error`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers:RootController"] = append(beego.GlobalControllerRouter["git.oschina.net/liukexing/RbacManager/controllers:RootController"],
		beego.ControllerComments{
			Method: "NotAccess",
			Router: `/notaccess`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

}
